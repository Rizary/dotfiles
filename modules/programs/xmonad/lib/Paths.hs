module Paths where

data Paths = Paths
  { xmobar :: String
  , terminal :: String
  }
