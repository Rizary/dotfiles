;;; tools/evilmagit.el --- Configuration for evil-magit
;;;
;;; Commentary:
;;;
;;; This file enables evil-magit integration  within emacs.
;;;
;;; Code:

;; Enable orgroam
(use-package evil-magit
  :demand t
  :after evil)

(provide 'evilmagit)
;;; evilmagit.el ends here
