;;; config/startup.el --- Configuration for startup process
;;;
;;; Commentary:
;;;
;;; This file modify how Emacs starts up
;;;
;;; Code:

;; Hide startup screen and messages
(setq inhibit-startup-screen t
      inhibit-startup-message t)



(provide 'startup) ;; to be available to another file
;;; startup.el ends here
