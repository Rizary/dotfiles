{
  font-0 = "Iosevka:style=Bold:pixelsize=12;3";
  font-1 = "Iosevka:style=Extra Bold;pixelsize=12;3";
  font-2 = "Iosevka:style=Light:pixelsize=12;3";
  font-3 = "Noto Sans:style=light:pixelsize=12;3";

  xfonts = "xft:Iosevka:style=bold:size=12,"
    + "xft:Iosevka:style=extra bold:size=12,"
    + "xft:Iosevka:style=light:size=12,"
    + "xft:Noto Sans:style=light;pixelsize=12";
}
