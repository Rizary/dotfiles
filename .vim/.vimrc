"source this files first before starting vimrc
source ~/dotfiles/.vim/autoload/pathogen.vim
"##############Pathogen initiation
call pathogen#infect()
filetype off
call pathogen#helptags()
"####################
source ~/dotfiles/.vim/startup/setting.vim
source ~/dotfiles/.vim/startup/mappings.vim


set rtp+=/usr/local/lib/python2.7/site-packages/powerline/bindings/vim/

"always show statusline
set t_Co=256
"Load Custom Setting

"source ~/dotfiles/.vim/startup/color.vim

