﻿"VIM SETTING

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

syntax on
filetype plugin indent on
set smartcase
" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
"set list listchars=tab:¦®,trail:¬,extends:»,precedes:«,eol:®

"set <leader> default '
let mapleader=','

set wrap
set number
set relativenumber
set backspace=indent,eol,start
set hlsearch
set laststatus=2
set title
"set textwidth=80
"set colorcolumn=+1
if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file endif set history=50		" keep 50 lines of command line history set ruler		" show the cursor position all the time set showcmd		" display incomplete commands
endif
set incsearch		" do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
:syntax enable
"":highlight Comment ctermfg=green guifg=green

set smartindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
set foldcolumn=1
set shiftwidth=4
set expandtab

"NERDTree settings
"
"autocmd vimenter * NERDTree

"let g:nerdtree_tabs_open_on_console_startup=1
let g:nerdtree_tabs_autoclose=1
"let g:nerdtree_tabs_open_on_new_tab=1
let NERDTreeMinimalUI=1
let NERDTreeDirArrows=0

"powerline setting for vim


