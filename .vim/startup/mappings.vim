"mappings arrow & NERDTree
no <right> :NERDTree<CR><C-w>w
no <left> <C-w>w:q<CR> 
no <up> ddkP 
no <down> ddp
ino <right> <ESC>:NERDTree<CR><C-w>wi
ino <left> <ESC><C-w>w:q<CR>
ino <up> <Nop>
ino <down> <Nop>
vno <up> <Nop>
vno <left> <Nop>
vno <right> <Nop>
vno <down> <Nop>

"mapping colons and brackets
imap <leader>' ''<ESC>i 
imap <leader>" ""<ESC>i 
imap <leader>( ()<ESC>i 
imap <leader>[ []<ESC>i 
imap <leader>{ {}<ESC>i 
imap <leader>< <><ESC>i

"mapping window tab
nnoremap <Tab> gt
nnoremap <S-Tab> gT
imap <S-Tab> <ESC>gti^

