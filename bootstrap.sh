###### Variables

dir=~/dotfiles
olddir=~/dotfiles_old
files=".vimrc"
vimdir=".vim"

###########

# backup old_dotfiles
echo "Creating $olddir for backup... wait a moment"
mkdir -p $olddir
echo " ...done"

#move .vim folder to old_dotfiles
echo "Changing to the $dir directory"
cd $dir
echo "...done"

mv ~/$files ~/dotfiles_old/
ln -s $dir/$files ~/$files
ln -s $dir/$vimdir ~/$vimdir

source ~/.vimrc
